//
//  MidiMessage.cpp
//  CommandLineTool
//
//  Copyright © 2015 Tom Mitchell. All rights reserved.
//  MidiMessage.cpp written by Will Fairman on 05/10/2015

#include "MidiMessage.hpp"
#include <cmath>

MidiMessage::MidiMessage()
    {
        number = 60;
        velocity = 127;
        channel = 1;
        
    }
MidiMessage::~MidiMessage ()
    {
        
    }

void MidiMessage::setNoteNumber (int value)
    {
        number = value;
    }

void MidiMessage::setVelocity (int velo)
    {
        velocity = velo;
    }

void MidiMessage::setChannel (int chan)
    {
        channel = chan;
    }

int MidiMessage::getNoteNumber() const
    {
        return number;
    }
float MidiMessage::getMidiNoteInHertz() const
    {
        return 440 * pow(2, (number-69) / 12.0);
    }

float MidiMessage::getVelocity() const
    {
        return velocity;
    }

float MidiMessage::getFloatVelocity() const
    {
        return velocity / 127;
    }
int MidiMessage::getChannel() const
    {
        return channel;
    }