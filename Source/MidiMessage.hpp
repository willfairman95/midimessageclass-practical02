//
//  MidiMessage.hpp
//  CommandLineTool
//
//  Created by Will Fairman on 05/10/2015.
//  Copyright © 2015 Tom Mitchell. All rights reserved.
//

//  MidiMessage.h written by Will Fairman on 5/10/15

#ifndef MidiMessage_hpp
#define MidiMessage_hpp

#include <stdio.h>

/** Class for storing and manipulating MIDI note messages*/

class MidiMessage
{
public:
    
    //Constructor
    MidiMessage();
    
    //Destructor
    ~MidiMessage();
    
    //Mutator. sets the MIDI note number of the message
    void setNoteNumber (int value);
    
    //Mutator. sets the velocity of the message
    void setVelocity (int velo);
    
    //Mutator. sets channel
    void setChannel (int chan);
   
    //Accessor. returns note number of the message
    int getNoteNumber() const;
    
    //Accessor. returns the MIDI note as a frequency
    float getMidiNoteInHertz() const;
    
    //Accessor. returns the velocity of the message
    float getVelocity() const;
    
    //Accessor. converts velocity to amplitude
    float getFloatVelocity() const;
   
    //Accessor. returns channel of the message
    int getChannel() const;
   
private:
    int number;
    float velocity;
    int channel;
};


#endif /* MidiMessage_hpp */
