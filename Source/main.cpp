//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include <cmath>
#include "MidiMessage.hpp"

int main (int argc, const char* argv[])
{

    MidiMessage note;
    float noteIn;
    
    //print default note number
    std::cout << "Default note number: " << note.getNoteNumber();
    
    //user note number
    std::cout << "\nEnter a note number: ";
    std::cin >> noteIn;//user input number
    note.setNoteNumber(noteIn);//setting note number what user specified
    std::cout << "\nNote number: " << note.getNoteNumber() << "\nFrequency: " << note.getMidiNoteInHertz() << "\nAmplitude: " << note.getFloatVelocity() << "\nChannel: " << note.getChannel();

    return 0;
}

